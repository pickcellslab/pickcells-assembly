+++

title ="Collaborative Development"
weight = 2

date = "2018-04-15"
creatorDisplayName = "Michael Jackson"

lastmodifierdisplayname = "Mike Jackson"
lastmod = "2018-08-01"

repo = "pickcells-assembly"
fileUrl = "https://framagit.org/pickcellslab/pickcells-assembly/tree/docs/docs/content/developers/collab_dev/_index.md"

tags = ["community", "develop"]

+++

## Contents

* [Introduction](#introduction)
* [What is core and non-core development?](#what-is-core-and-non-core-development)
* [Open, by default](#open-by-default)
* [Source code](#source-code)
* [Coding standards](#coding-standards)
* [Reuse not repetition](#reuse-not-repetition)
* [Managing multiple developers and version control](#managing-multiple-developers-and-version-control)
* [Build artefacts](#build-artefacts)
* [Release versioning](#release-versioning)
* [Testing](#testing)
* [Continuous integration](#continuous-integration)
* [Issue tracker](#issue-tracker)
* [PickCells Colony forum](#pickcells-colony-forum)

## Introduction

In this page we document our approach to developing PickCells.

## What is core and non-core development?

Core development of PickCells relates to development of the PickCells architecture and framework, developments which are held within the PickCells [source code repositories](https://framagit.org/pickcellslab) and are distributed as part of PickCells. Generally, core development includes:

* Developing features which provide new functionality to other modules. For example, see the issue [Provide API to easily obtain segmented object thumbnails](https://framagit.org/pickcellslab/pickcells-api/issues/9).
* Developments which affect the behaviour of the PickCells framework.
* Developments which could potentially lead to changes in APIs.
* Developments whichy record user's actions e.g. see the PickCells colonly forum discussion [Auto-generated pipelines](https://colony.pickcellslab.org/t/auto-generated-pipelines/27), or the issue [Add Trigger system when certain objects are deleted from the database](https://framagit.org/pickcellslab/foundationj/issues/5).

There are also a number of 'core-related' projects:

* [pickcells-j3d](https://framagit.org/pickcellslab/pickcells-j3d): visualisation modules based on [Java3D](https://en.wikipedia.org/wiki/Java_3D).
* [pickcells-jzy3d](https://framagit.org/pickcellslab/pickcells-jzy3d): visualisation modules based on [Jzy3D](http://www.jzy3d.org/).
* [pickcells-rjava](https://framagit.org/pickcellslab/pickcells-rjava): Allows R integration into PickCells using [rJava](https://cran.r-project.org/web/packages/rJava/index.html).
* [pickcells-jfree](https://framagit.org/pickcellslab/pickcells-jfree): visualisation modules based on [JFreeChart](http://www.jfree.org/jfreechart/).
* [pickcells-prefuse](https://framagit.org/pickcellslab/pickcells-prefuse): visualisation modules based on [Prefuse](http://prefuse.org/).

Non-core development relates to development done by third-parties e.g. development of plugins, other extensions or tutorials. Non-core components are not distributed as a part of PickCells.

## Open, by default

To leverage the power of open source, and to build and grow a community, the default for all discussions is to use publicly-visible source code repositories, a web site and a forum. We adopt the philosopgy that everything is open unless there is a good reason for it to be private.

Developers of PickCells components should be open to inviting others to contribute. This includes welcoming users and developers, acknowledging and working on pull requests, encouraging improvements, working together, enhancing upon each others' work, sharing insights, etc.

For information on what the contributions we welcome from the community, see [How to Contribute](/community/contribpolicy).

## Source code

PickCells is written in Java.

The PickCells source code has the following features:

* PickCells is written entirely in Java.
* Apache [Maven](https://maven.apache.org) is use as PickCells' build and dependency management tool.
* The code in the [pickcells-assembly](https://framagit.org/pickcellslab/pickcells-assembly) repository assembles PickCells from its constituent components held within its other Git repositories. pickcells-assembly holds a Maven aggregator POM file.
* The "master" branch of each repository is considered release-ready at all times, meaning it compiles with passing tests, and is ready for downstream consumption.
* PickCells maintainers may make commits and release new versions of the components as needed, so that PickCells as a whole continues to work as intended.

The PickCells source code is held within several Git [source code repositories](https://framagit.org/pickcellslab), hosted within a [GitLab](https://gitlab.com) service provided by [Framgit](https://framagit.org/).

PickCells source code is freely accessible:

* Anyone is able to clone and fork the repositories.
* Permission to merge or push to the repositories is by invitation-only.
* Anyone is able to submit pull requests to the repositories.
* PickCells source code is licenced under the [GNU General Public Licence v3](https://www.gnu.org/licenses/gpl-3.0.en.html).
* Components contributed to PickCells must comply with certain conditions (see our [Contributor Licence Agreement](/community/contribpolicy/#Licence)).

The [PickCells Developer Guide](/developers/dev_core/) describes how to build the complete code, as well as any dependencies and requirements.

## Coding standards

Our code aims to conform to the [Google Java Style Guide](https://google.github.io/styleguide/javaguide.html).

## Reuse not repetition

Whenever possible, we aim to use third-party packages (licences permitting) and to improve the existing PickCells source code. We only write code from scratch if absolutely necessary.

## Managing multiple developers and version control

A number of developers work on PickCells. We exploit the features of version control in Git and Framagit's GitLab service to manage collaborative development of source code.

If you are unfamiliar with Git then we recommend [Software Carpentry](https://software-carpentry.org)'s introduction to [Version Control with Git](https://swcarpentry.github.io/git-novice/). 

To manage development across multiple developers, a "forking workflow" is used. Each developer creates a "fork" (or "clone"), in Framagit, of the PickCells repositories they want to work upon. They then "clone" this to their local computer. The developer works on their local version, commiting their changes, and "pushing" these, at regular intervals to their version on Framagit. If they want their changes merged into the official PickCells repositories, they issue a "pull request". The PickCells maintainers will then review this "pull request" and, if happy, "merge" this into the official PickCells repositories, or provide feedback if there are issues with the "pull request" (e.g. it is missing documentation or has an obvious bug). This approach to development allows each developer to work independently on their local copies, and to make changes for these, without the risk of them accidently introducing bugs into the official PickCells repositories.

For a fuller description of this model, see Atlassian's description of the [Forking Workflow](https://www.atlassian.com/git/tutorials/comparing-workflows/forking-workflow).

For an example of its use with PickCells, see [Get PickCells repositories](/developers/dev_core/setup/#get-pickcells-repositories) and [Using Git and Framagit](/developers/dev_core/day_to_day/#using-git-and-framagit).

## Build artefacts

Build artefacts, built and used by Maven, are published on a PickCell's specific deployment of the [Apache Archiva](https://archiva.apache.org/index.cgi) build repository manager, [archiva.pickcellslab.org](https://archiva.pickcellslab.org).

## Release versioning

For releases, PickCells uses [semantic versioning](https://semver.org/) e.g. `PickCells-0-7-3-alpha.zip`.

## Testing

Ideally, all compoents should have a suite of unit, integration and regression tests.

For an introduction to testing concepts, see [Testing](https://en.wikibooks.org/wiki/Introduction_to_Software_Engineering/Testing) in [Introduction to Software Engineering](https://en.wikibooks.org/wiki/Introduction_to_Software_Engineering) on Wikibooks. 

Tests in PickCells are implemented using the  [JUnit 5](https://junit.org/junit5/) unit testing framework. For an introduction to testing with JUnit 5, see the [JUnit 5 Tutorial](https://howtodoinjava.com/junit-5-tutorial/).

[Mockito](http://site.mockito.org/) is used for mock object-based testing.

We also recommend the use of [code coverage](https://en.wikipedia.org/wiki/Code_coverage) tools such as [Jacoco](http://www.eclemma.org/jacoco/) and [Cobertura](http://cobertura.github.io/cobertura/). These tools identify the code that is executed as a side-effect of running tests and so can help to identify code that is not currently being tested.

## Continuous integration

Continuous integration is a practice whereby developers are encouraged to commit changes to their code regularly. These commits trigger an automated build of the software and a run of any tests. Continuous integration software provides the underlying infrastructure for detecting changes to source code repositories, running scripts in response to these changes and publishing the results of build-and-test runs.

For automated testing, PickCells uses [GitLab Continuous Integration](https://about.gitlab.com/features/gitlab-ci-cd/) (GitLab CI) freely provided by Framagit to all projects hosted there. Not only can GitLab CI be used to run tests, it can also be used to run other jobs and to build artefacts e.g. documentation (it is used to build this web site, for example).

## Issue tracker

PickCells uses an [issue tracker](https://framagit.org/groups/pickcellslab/-/issues) to record feature requests and bugs. These can also be raised and discussed in the PickCells Colony forum, but for milestones and tracking purposes, the issue tracker is the entry point for feature requests and bugs. The issue tracker is an overarching issue tracker associated with the PickCells project as a whole. It provides a view onto all repository-specific issue trackers.

## PickCells Colony forum

The [PickCells Colony forum](https://colony.pickcellslab.org/) is the place to find help and share ideas with other members of the PickCells community. There you can find out about and discuss research, how-tos, tutorials, feature requests, bugs, news and events.

The PickCells Colony forum is publicly-readable. You need to register to create new posts. Currently registration is by invitation only. However, there will be a sign-up option added soon.

The PickCells Colony forum is implemented using [Discourse](https://www.discourse.org/).
