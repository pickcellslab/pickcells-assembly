+++

title ="Architecture And Concepts"
weight = 1

date = "2018-07-23"
creatorDisplayName = "Guillaume Blin"

lastmodifierdisplayname = "Guillaume Blin"
lastmod = "2019-01-31"

repo = "pickcells-assembly"
fileUrl = "https://framagit.org/pickcellslab/pickcells-assembly/blob/docs/docs/content/developers/architecture/_index.md"

+++

{{% panel theme="warning" header="Draft" %}}

This page is still a draft, however some useful links are readily available, pealse see below

{{% /panel %}}



Architecture and Concepts:

  * FoundationJ
  
    * [Component Based Design](/developers/architecture/foundationj/dependency_injection) (Dependency Injection)
    * [Accessing and querying the database](/developers/architecture/foundationj/query_factory)
    * [The MetaModel](/developers/architecture/foundationj/metamodel)

  * PickCells API (located in the pickcells-api repo - brief description + link to creating plugins)
  
    * Data Model (Image, Nucleus, etc... GenericSegmented)
    * Charts
    * Data Picking
    * Utilities (geometry, data fitting, triangulation, etc...)
    * Working with Images (I/O - link to ImgLib2)
    * PickCells Scopes
				 


