+++

title ="Set up a PickCells development environment"
weight = 2

date = "2018-04-15"
creatorDisplayName = "Michael Jackson"

lastmodifierdisplayname = "Mike Jackson"
lastmod = "2018-08-02"

repo = "pickcells-assembly"
fileUrl = "https://framagit.org/pickcellslab/pickcells-assembly/tree/docs/docs/content/developers/dev_core/setup/_index.md"

tags = ["develop"]

+++

This page describes how to install third-party tools and packages required to develop PickCells and how to get the PickCells source code. 

---

## Contents

* [Terminology](#terminology)
* [Install prerequisites](#install-prerequisites)
  - [Install Java 1.8 JDK](#install-java-1-8-jdk)
     - [Install OpenJDK](#install-openjdk)
     - [Install Oracle Java](#install-oracle-java)
  - [Install Maven 3.3+](#install-maven-3-3)
  - [Install GraphViz](#install-graphviz)
  - [Install Git](#install-git)
* [Create and register an SSH key with Framagit](#create-and-register-an-ssh-key-with-framagit)
* [Get PickCells repositories](#get-pickcells-repositories)
  - [Fork a PickCells repository](#fork-a-pickcells-repository)
  - [Clone a PickCells repository](#clone-a-pickcells-repository)
* [Build PickCells release](#build-pickcells-release)
* [Build PickCells release from the repositories](#build-pickcells-release-from-the-repositories)

---

## Terminology

In the following, when we say "open a bash window":

* Ubuntu users, open a terminal window.
* Windows 7 users, open Git Bash.

---

## Install prerequisites

We assumes you have already downloaded, installed and run PickCells, as described in our documentation on how to [Get Started](/getstarted/).

### Install Java 1.8 JDK

You can use either [OpenJDK](http://openjdk.java.net) or [Oracle Java](https://www.oracle.com/uk/java/index.html)

64-bit Java is highly recommended for 64-bit operating systems.

{{% notice warning %}}
Use the newest version of Java 1.8 JDK you can. There are issues with older versions of Java that have been resolved with more recent versions. For example, the authors have encountered issues with Java JDK 1.8.0_20 on Macintosh but no issues with with OpenJDK Java JDK 1.8.0_191 or Java JDK 1.8.0_201 on Macintosh 
{{% /notice %}}

PickCells has not been tested on Java 1.9.

For full installation instructions for Java, consult the relevant third-party documentation.

#### Install OpenJDK

**Ubuntu users**

1. Run:

    ```
    sudo apt-get install openjdk-8-jdk
    javac -version
    ```

2. You should see something like:

    ```
    javac 1.8.0_131
    ```

3. Edit `/etc/environment`:

    ```
    sudo nano /etc/environment
    ```

4. Add the line:

    ```
    JAVA_HOME="/usr/lib/jvm/java-8-openjdk-amd64/"
    ```

5. Run:

    ```
    source /etc/environment
    echo $JAVA_HOME
    ```

6. You should see something like:

    ```
    /usr/lib/jvm/java-8-openjdk-amd64/
    ```

#### Install Oracle Java

**Windows 7 users**

Install Oracle Java

1. Visit Oracle [Java SE Development Kit 8 Downloads](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html).
2. Click Accept License Agreement. 
3. Download Windows x64 version e.g. `jdk-8u144-windows-x64.exe`. 
4. Double-click `exe` e.g. `jdk-8u144-windows-x64.exe`. 
5. Follow installer instructions. 
6. Open a command prompt. 
7. Run: 

    ```
    "C:\Program Files\Java\jdk1.8.0_144\bin\javac" -version
    ```

8. You should see something like:

    ```
    javac 1.8.0_144
    ```

Update environment variables:

1. Select **Start** => **Control Panel**.
2. Enter `environment` in search field.
3. Select Edit the system environment variables.
4. Click Advanced tab
5. Click Environment Variables...
6. Under User variables, click New...
7. Enter Variable name: `JAVA_HOME`
8. Enter Variable value: `C:\Program Files\Java\jdk1.8.0_144\`
9. Click OK
10. Select PATH
11. Click Edit...
12. Add to the end of the value: 

    ```
    ;C:\Program Files\Java\jdk1.8.0_144\bin
    ```

13. Click OK
14. Click OK
15. Click OK
16. Open a command prompt.
17. Run:

    ```
    javac -version
    ```

18. You should see something like:

    ```
    javac 1.8.0_144
    ```

### Install Maven 3.3+

PickCells source code is built and tested using the [Apache Maven](https://maven.apache.org/) software project managment tool.

To install Maven may require that you have administrator access to install and configure software. If you do not have administrator access, a local system administrator may be able to give you this, or install Maven for you.

**Ubuntu users**

1. Run:

    ```
    sudo apt-get install maven
    mvn -v
    ```

2. You should see something like:

    ```
    Apache Maven 3.3.9
    Maven home: /usr/share/maven
    Java version: 1.8.0_131, vendor: Oracle Corporation
    Java home: /usr/lib/jvm/java-8-openjdk-amd64/jre
    Default locale: en_US, platform encoding: UTF-8
    OS name: "linux", version: "4.8.0-36-generic", arch: "amd64", family: "unix"
    ```

The value of `Java version` and `Java home` should match the location and version of Java you installed earlier.

**Windows 7 users**

1. Visit [Downloading Apache Maven](https://maven.apache.org/download.cgi).
2. Download binary zip archive e.g. `apache-maven-3.5.0-bin.zip`.
3. Right-click zip archive e.g. `apache-maven-3.5.0-bin.zip`.
4. Select Extract All...
5. Move `apache-maven-3.5.0-bin\apache-maven-3.5.0` to `C:\Program Files`.

Update environment variables:

1. Select **Start** => **Control Panel**.
2. Enter `environment` in search field.
3. Select Edit the system environment variables.
4. Click Advanced tab
5. Click Environment Variables...
6. Under System variables, click New...
7. Select Path
8. Click Edit...
9. Add to the end of the value:

    ```
    ;C:\Program Files\apache-maven-3.5.0\bin
    ```

10. Click OK
11. Click OK
12. Click OK
13. Open a command prompt.
14. Run:

    ```
    mvn -v
    ```

15. You should see something like:

    ```
    Apache Maven 3.5.0 (ff8f5e7444045639af65f6095c62210b5713f426; 2017-04-03T20:39:06+01:00)
    Maven home: C:\Program Files\apache-maven-3.5.0\bin\..
    Java version: 1.8.0_144, vendor: Oracle Corporation
    Java home: C:\Program Files\Java\jdk1.8.0_144\jre
    Default locale: en_GB, platform encoding: Cp1252
    OS name: "windows 7", version: "6.1", arch: "amd64", family: "windows"
    ```

The value of `Java version` and `Java home` should match the location and version of Java you installed earlier.

### Install GraphViz

[GraphViz](http://www.graphviz.org/) can be used to create images of Maven dependency trees.

To install GraphViz may require that you have administrator access to install and configure software. If you do not have administrator access, a local system administrator may be able to give you this, or install GraphViz for you.

Any recent version of GraphViz should be OK.

**Ubuntu users**

1. Run:

    ```
    sudo apt-get install graphviz
    dot -V
    ```

2. You should see something like:

    ```
    dot - graphviz version 2.38.0 (20140413.2041)
    ```

**Windows 7 users**

1. Visit GraphViz [Download](http://www.graphviz.org/download/).
2. Download a stable Windows MSI file e.g. `graphviz-2.38.msi`.
3. Double-click MSI file e.g. `graphviz-2.38.msi`.
4. Accept installer defaults.
5. Select Start.
6. Enter `gvedit.exe`.
7. GVEdit should appear

Update environment variables:

1. Select **Start** => **Control Panel**.
2. Enter `environment` in search field.
3. Select Edit the system environment variables.
4. Click Advanced tab
5. Click Environment Variables...
6. Under System variables, click New...
7. Select Path
8. Click Edit...
9. Add to the end of the value:

    ```
    ;C:\Program Files (x86)\Graphviz2.38\bin
    ```

10. Click OK
11. Click OK
12. Click OK
13. Open a command prompt.
14. Run:

    ```
    dot -v
    ```

15. You should see something like:

    ```
    dot - graphviz version 2.38.0 (20140413.2041)
    ```

### Install Git

[Git](https://git-scm.com/) is a source code version control tool.

To install Git requires that you have administrator access to install and configure software. If you do not have administrator access, a local system administrator may be able to give you this, or install Git for you.

Any recent version of Git should be OK.

**Ubuntu users**

1. Run:

    ```
    sudo apt-get install git
    git --version
    ```

2. You should see something like:

    ```
    git version 2.7.4
    ```

**Windows 7 users**

[Git for Windows](https://git-for-windows.github.io/) is recommended. It provides both Git and a bash shell.

1. Visit [Git for Windows](https://git-for-windows.github.io/).
2. Click the Download button.
3. Double-click installer `.exe` e.g. `Git-2.15.1.2-64-bit.exe`.
4. Git Bash and Git GUI will both be installed.
5. Open the Git Bash command prompt.
6. Run:

    ```
    git --version
    ```

7. You should see something like:

    ```
    git version 2.15.1.windows.2
    ```

8. Set your user name and email:

    ```
    git config --global user.name "<YOUR-NAME>"
    git config --global user.email "<YOUR-EMAIL-ADDRESS>"
    ```

---

## Create and register an SSH key with Framagit

1. Open a bash window.
2. Run:

    ```
    ssh-keygen -t rsa
    ```

3. When prompted, enter a pass-phrase.
4. A public key (`id_rsa.pub`) and private key (`id_rsa`) will be created in `.ssh`, which you can check by running:

    ```
    ls ~/.ssh/
    ```

5. Log in to [Framagit](https://framagit.org/).
6. Select user profile (avatar icon on top right) => **Settings**.
7. Click SSH Keys (key icon).
8. Paste in the contents of the file `id_rsa.pub` into Key.
9. Title should fill itself in automatically.
10. Click Add key.

---

## Get PickCells repositories

For more information of what each of the PickCells components does, see their individual README.md files which can be found at the top of each component repository, e.g. https://framagit.org/pickcellslab/foundationj/blob/master/README.md for the foundationj component.

For a public, open source project such as PickCells, it is useful if developers are able to work on their own personal copies of the original, public server-side repository, without the danger of deleting anything by mistake in the latter.

Only the PickCells maintainers are able to make change to the code in the PickCells source code repositories. Other developers work on their own forked versions of these repositories. For more information on this concept, the "forking workflow" please see [Managing multiple developers and version control](/developers/collab_dev/#managing-multiple-developers-and-version-control) in our page on [Collaborative Development](/developers/collab_dev).

Here we describe how to get access to PickCells source code using the "forking workflow".

### Fork a PickCells repository

1. Log in to [Framagit](https://framagit.org/).
2. Visit the [pickcellslab](https://framagit.org/pickcellslab/) project.
3. Go to the repository you want to work upon e.g. [pickcells-assembly](https://framagit.org/pickcellslab/pickcells-assembly).
4. Click the "Fork" button.
5. Framagit will ask you to select a namespace in which to fork the repository. Click the one corresponding to your user name.
6. Framagit should fork the repository into your own personal space. Once complete you should see a message:

    ``` 
    The project was successfully forked.
    ```
7. Your fork will be at the URL `https://framagit.org/<YOUR-USER-NAME>/<REPOSITORY-NAME>` e.g. `https://framagit.org/jo12345/pickcells-assembly`.

### Clone a PickCells repository

Now you can clone your PickCells repository onto your local computer.

1. Open a bash window.
2. Run:

    ```
    git clone git@framagit.org:<YOUR-USER-NAME>/<REPOSITORY-NAME>.git
    ```

    For example:

    ```
    git clone git@framagit.org:jo12345/pickcells-assembly.git
    ```

3. When prompted, enter your passphrase that you provided when creating your SSH key.
4. Change into the folder with the clone;

    ```
    cd <REPOSITORY-NAME>
    ```

    For example:

    ```
    cd pickcells-assembly
    ```

5. Create an 'upstream remote' to be able to track, and access, developments of the official PickCells repository you forked:

    ```
    git remote add upstream https://framagit.org/pickcellslab/<REPOSITORY-NAME>
    ```

    For example:

    ```
    git remote add upstream https://framagit.org/pickcellslab/pickcells-assembly
    ```

6. Check the currently configured remote repositories for your fork. You should see something like, for example:
 
    ```
    git remote -v
    origin                     git@framagit.org:<YOUR-USER-NAME>/pickcells-assembly.git (fetch)
    origin                     git@framagit.org:<YOUR-USER-NAME>/pickcells-assembly.git (push)
    upstream                   https://framagit.org/pickcellslab/pickcells-assembly (fetch)
    upstream                   https://framagit.org/pickcellslab/pickcells-assembly (push)
    ```

7. Check if the repository uses [git submodules](https://git-scm.com/docs/git-submodule) by checking if it has a `.gitmodules` file. If so then initialise the submodule:

    ```
    ls .gitmodules
    ```

    If you get 
    
    ```
    .gitmodules
    ```

    then initialise the submodule:

    ```
    git submodule init
    git submodule update
    ```

    Currently this is only required by the [docs](https://framagit.org/pickcellslab/docs) repository.

---

## Build PickCells release

To build a PickCells release:

1. Fork and clone all the PickCells repositories, if you have not already done so.
2. Run:

    ```
    cd pickcells-assembly
    mvn install
    ```

If any problems arise, see [Troubleshooting](/developers/dev_core/troubleshooting).

---

## Build PickCells release from the repositories

To build a PickCells release from the repositories:

1. Fork and clone all the PickCells repositories.
2. In each of the following repositories:

    ```
    foundationj/
    pickcells-api/pickcells-api-build/
    pickcells-j3d/
    pickcells-jfree/
    pickcells-jzy3d/
    pickcells-neodb/
    pickcells-prefuse/
    pickcells-rjava/
    pickcells-essentials/
    ```

    Run:

    ```
    mvn install
    ```

3. In `pickcells-assembly/`, run:

    ```
    mvn package
    ```

4. A PickCells release will be available in, for example, `pickcells-assembly/target/PickCells-0.7.0.zip`.

If any problems arise, see [Troubleshooting](/developers/dev_core/troubleshooting).
