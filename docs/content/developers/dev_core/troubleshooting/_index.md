+++

title ="Troubleshooting"
weight = 5

date = "2019-01-23"
creatorDisplayName = "Mike Jackson"

lastmodifierdisplayname = "Mike Jackson"
lastmod = "2019-01-23"

repo = "pickcells-assembly"
fileUrl = "https://framagit.org/pickcellslab/pickcells-assembly/tree/docs/docs/content/developers/dev_core/troubleshooting/_index.md"

tags = ["develop"]

+++

This page describes how to troubleshoot day-to-day development tasks on the PickCells source code.

## Contents

* [Failed to deploy artifacts... Not authorized](#failed-to-deploy-artifacts-notauthorized)
* [Could not transfer artifact... Not authorized](#could-not-transfer-artifact-not-authorized)
* [Could not resolve dependencies for project](#could-not-resolve-dependencies-for-project)
* [sun.security.provider.certpath.SunCertPathBuilderException](#sun-security-provider-certpath-suncertpathbuilderexception)
* [no suitable method found for collect](#no-suitable-method-found-for-collect)

---

## Failed to deploy artifacts... Not authorized

If, when running `maven deploy`, you get an error matching the following pattern:

```
[ERROR] Failed to execute goal ...:deploy (default-deploy) on project ...:
Failed to deploy artifacts:
Could not transfer artifact ... from/to ... 
(https://archiva.pickcellslab.org/repository/.../):
Failed to transfer file: https://archiva.pickcellslab.org/repository/....
Return code is: 401, ReasonPhrase: Unauthorized. -> [Help 1]
```

Then this is because you are not authorised to upload artifacts to the PickCells Apache Archiva repository [archiva.pickcellslab.org](https://archiva.pickcellslab.org/).

---

## Could not transfer artifact... Not authorized 

If, when running Maven, you get an error matching the following pattern:

```
Could not transfer artifact ... from/to ... : Not authorized,
ReasonPhrase:Unauthorized. -> [Help 1]
```

This could be due to the guest account for the PickCells Apache Archiva repository [archiva.pickcellslab.org](https://archiva.pickcellslab.org/) having reset. This is a known issue with Apache Archiva (see [Fixing the Archiva Guest User](https://github.com/isetbio/RemoteDataToolbox/wiki/Fixing-the-Archiva-Guest-User)). If this problem arises, please see [Get Help](/help) to get in touch.

---

## Could not resolve dependencies for project 

If, when running Maven, you get an error matching the following pattern:

```
[ERROR] Failed to execute goal on project ...:

... Could not resolve dependencies for project ...:...:

... Could not find artifact PRODUCT:jar:X.y... in ...
```

This could be due to missing artifacts in the PickCells Apache Archiva repository [archiva.pickcellslab.org](https://archiva.pickcellslab.org/). [Browse Archiva](https://archiva.pickcellslab.org/#browse/org.pickcellslab.pickcells) to see if the artifact is indeed missing, and if so, please see [Get Help](/help) to get in touch.

---

## sun.security.provider.certpath.SunCertPathBuilderException

If, when running Maven, you get an error similar to the following:

```
[WARNING] Could not transfer ... from/to
pickcells-snapshots (https://archiva.pickcellslab.org/repository/pickcells-snapshots/): 
... sun.security.validator.ValidatorException: PKIX path building failed:
sun.security.provider.certpath.SunCertPathBuilderException:
unable to find valid certification path to requested target
```

then check your Java JDK version. There are issues with older versions of Java that have been resolved with more recent versions. For example, the authors have encountered this issue with Java JDK 1.8.0_20 on Macintosh but not with OpenJDK Java JDK 1.8.0_191 or Java JDK 1.8.0_201 on Macintosh

If you still have problems then:


1. Get [SSLPoke](https://github.com/MichalHecko/SSLPoke)

    ```
    git clone https://github.com/MichalHecko/SSLPoke
    ```

2. Compile SSLPoke:

    ```
    javac -sourcepath src/main/java/ src/main/java/sk/mhecko/ssl/SSLPoke.java -d .
    ```

3. Run SSLPoke on a web page URL e.g. EPCC:

    ```
    java sk.mhecko.ssl.SSLPoke www.epcc.ed.ac.uk 443
    ```

    You should see:

    ```
    Successfully connected
    ```

4. Run SSLPoke on Archiva's URL:

    ```
    java SSLPoke archiva.pickcellslab.org 443
    ```

5. If you get an error similar to the following:

    ```
    ...
    sun.security.validator.ValidatorException: PKIX path building failed: 
    sun.security.provider.certpath.SunCertPathBuilderException: unable to 
    find valid certification path to requested target
         at sun.security.validator.PKIXValidator.doBuild(PKIXValidator.java:387)
    ...
    ```

    then continue.

6. Open a web browser.
7. Visit https://archiva.pickcellslab.org/
8. Download the certificate. For example, using Firefox:
   - Left click on green padlock by the URL.
   - Select Connection => More Information => View Certificate => Details => Export
   - Save the certificate to `archivapickcellslaborg.crt`
9. Import the certificate into your JDK's `cacerts-file` (you may have to do a web search to find out where this is for your operating system). For example:

    ```
    sudo keytool -import -alias archiva_server -keystore /<PATH>/jdk1.8.0_20.jdk/Contents/Home/jre/lib/security/cacerts-file archivapickcellslaborg.crt
    ```

10. If you get prompted for a password but have never set one for the keytool,c create a password via the following instructions (from Oracle's [How to Change the Java Keystore Password](https://docs.oracle.com/cd/E19957-01/817-3331/6miuccqo3/index.html)):

    ```
    sudo keytool -storepasswd -keystore /<PATH>/jdk1.8.0_20.jdk/Contents/Home/jre/lib/security/cacerts
    ```

  - Use `changeit` as the initial password and something you'll remember for the new password.

11. Rerun SSLPoke:

    ```
    java SSLPoke archiva.pickcellslab.org 443
    ```

    You should see:

    ```
    Successfully connected
    ```

12. Rerun your Maven command.

---

## no suitable method found for collect

If, when running Maven, you get an error matching the following pattern:

```
no suitable method found for 
collect(java.util.stream.Collector<java.lang.Object,capture#1 of 
?,java.util.List<java.lang.Object>>)
     method 
```

Then your version of Java JDK is too old. This is a bug in older versions of Java 1.8 (see, for example, StackOverflow [Why this code compiles with jdk8u45 and above but not with jdk8u25?](https://stackoverflow.com/questions/37368060/why-this-code-compiles-with-jdk8u45-and-above-but-not-with-jdk8u25)).
