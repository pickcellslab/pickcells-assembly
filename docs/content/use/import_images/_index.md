+++

title = "Import images"
weight = 3

lastmod = "2018-12-03" 

repo = "pickcells-assembly" 
fileUrl = "https://framagit.org/pickcellslab/pickcells-assembly/blob/docs/docs/content/use/import_images.md" 

+++

## Contents

* [Import raw images](#import-raw-images)
  - [Select raw images](#select-raw-images)
  - [Enter channel names and colours for raw images](#enter-channel-names-and-colours-for-raw-images)
* [Import segmented images](#import-segmented-images)
* [Import images from multiple libraries and/or folders](#import-images-from-multiple-libraries-and-or-folders)
* [Constraints on images](#constraints-on-images)
* [View image location in file system](#view-image-location-in-file-system)

---

## Import raw images

{{% notice note %}}
You **cannot** import images into an existing PickCells experiment. For more information, see the [FAQ: Can I import images to a PickCells experiment which already contains images](/help/faq/#can-i-import-images-to-a-pickcells-experiment-which-already-contains-images)
{{% /notice %}}

To import raw images, select **Files** => **Import...** => **Colour Images**.

This opens the import dialog. The import dialog offers two options:

* Click **Add Library** to select a library file, see the images it contains, then select which of the images you want to import. Library files are commonly created by microscopy software and contain a collection of images. Common examples include Leica `.lif` and Zeiss `.lsm` files.
* Click **Add Images**  to select raw image files held within a folder on your file system.

### Select raw images

A file browser will appear:

* Browse to the folder containing your images.
* Select `ics` files. To select only these files, hold down the Ctrl key and click each `ics` file.
* Click **Choose**.

Once you have selected the images to load, a selected images dialog will display a thumbnail and a short description of each selected image.

If you are happy with the selected images, click **OK**.

### Enter channel names and colours for raw images

The channel names dialog will appear which allows you, for each channel in the images, to select the following:

* **Name**: A name for the channel. Typically this will be the name of the protein strained for.
* **LUT**: A colour scheme for the channel. LUT stands for Lookup Table which defines a range of shades for a specific colour.

When you are done, click **OK**.

{{% notice note %}} 
Channel numbers are zero-indexed. The first channel has index 0, the second channel has index 1 etc. 
{{% /notice %}} 
 
PickCells will import your selected images and a progress bar will appear while it does so.

{{% notice note %}} 
Importing images is dependent upon both the number and size of the images and may take some time. 
{{% /notice %}} 
 
{{% notice tip %}} 
You can check the progress of the import by navigating to your experiment's database folder.

To open your experiment's database folder, select **Locations** => **Database Folder** 
 
This folder will contain one `.ics` and one `.ids` file for each imported image. 
{{% /notice %}} 
 
When the images have been imported, the MetaModel View will update with objects representing the images. 

{{% notice note %}} 
PickCells does **not** show the images themselves, once they have been loaded. 
{{% /notice %}} 

---

## Import segmented images

To import images of segmentation results created using other tools, select **Files** => **Import...** => **Segmented Images**.

The steps to select segmented images to import are the same as already described in [Select raw images](#select-raw-images).

{{% notice note %}} 
The thumbnails of the images will show a red circle with a bar (like a 'stop' sign). This means that PickCells cannot show the thumbnail. However, the images themselves **are** OK to be imported. 
{{% /notice %}} 

PickCells will check that the segmented images you import have the same dimensions as the raw images you have already imported.

---

## Import images from multiple libraries and/or folders

To import images from multiple libraries and/or folders, click **Add Library** or **Add Images** again when in the import dialog.

To view the images you have selected from a specific library or folder, click the drop-down list at the top-left of the import dialog.

---

## Constraints on images

While there is no limit to the number of images or number of biological conditions that can be analysed in PickCells, there are a few constraints on the images you select:

* Selected images must have a consistent number of channels.
* Selected images can have different sizes (width, height, depth) but their resolution (voxel size) and dimensionality (2D - 3D - 4D, etc.) must be the same. To be able to compare different images, the acquisition parameters need to be consistent and so PickCells tests for this condition.
* Names of selected images should be such that a pattern can be used to identify replicates and conditions (this is not mandatory, but you will find it very useful).

If any of these constraints are not met, then PickCells will notify you.

---

## View image location in file system

You can view the location of raw and segmented images you have imported by navigating to your experiment's database folder.

To open your experiment's database folder, select **Locations** => **Database Folder**

This folder will contain one `.ics` and one `.ids` file for each imported image.
