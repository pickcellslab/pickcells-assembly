+++

title ="Tutorials"

lastmod = "2018-08-01"  

weight = 5
  
repo = "pickcells-assembly"  
fileUrl = "https://framagit.org/pickcellslab/pickcells-assembly/blob/docs/docs/content/use/tutorials/_index.md"  

+++

The following tutorials and HOW-TOs are available:

| Tutorial | Modules | Location |
| -------- | ------- | -------- |
| [Nuclear markers analysis](/getstarted/walkthrough) | Intrinsic Features, Histogram | Website |
| [Creating scatter plots, filters, pie charts and image overlays](/getstarted/walkthrough_plots_filters_charts) | 2D Scatter (Bubble) Plot, Pie Chart, Image | Website |
| [Segmentation editor](/use/features/segmentation/seg_editor) | Segmentation Editor | Website |
| [Counting iPSC colonies after whole-well imaging](https://colony.pickcellslab.org/t/counting-ipsc-colonies-after-whole-well-imaging/42/2) | Segmentation, Instinsic Features | Colony |
| [3D regions analysis](https://colony.pickcellslab.org/t/3d-regions-analysis/36) | Segmentation, Segmentation Editor, Intrinsic Features | Colony |
