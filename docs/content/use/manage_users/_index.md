+++

title = "Manage users"
weight = 1

lastmod = "2018-12-03"
  
repo = "pickcells-assembly"
fileUrl = "https://framagit.org/pickcellslab/pickcells-assembly/blob/docs/docs/content/use/manage_users.md"

+++

## Contents

* [Administrator account](#administrator-account)
* [Create a new user](#create-a-new-user)
* [Log in as a new user](#log-in-as-a-new-user)
* [Delete a user](#delete-a-user)
* [Reset all user accounts](#reset-all-user-accounts)
* [Log off](#log-off)

---

## Administrator account

The first user to log in to a new PickCells installation will automatically receive administrator rights. The administrator is the only user who can create and delete other user accounts.  

{{% notice tip %}}
**Keep a note of your administrator credentials** as only the administrator can manage other users.
{{% /notice %}}

---

## Create a new user

To create a new user:

* Click the Go to the User Overview {{< icon name="fa-user-o">}} icon, located on the bottom-left of the PickCells window, to go to the user screen.
* Click the Add a new user {{< icon name="fa-user-plus">}} icon, located on the right-hand side in the My Experiments panel.

The New User... dialog will appear:

* Let the user enter their user name and password.
* Click **Login**.

The user screen will appear and the new user will be visible in the Managed Users panel. 

---

## Log in as a new user

To login using a new user account:

* Click the Log off {{<icon name="new-window">}} icon. This will automatically close all opened experiments and return you to the welcome screen.
* Click **Login**.

The Login dialog will appear:

* Enter the new user name and password.
* Click **Login**.

---

## Delete a user

Deleting a user both deletes the user as well as all the experiments that belong to the user. If any of the experiments were shared with other users, these shared experiments will no longer be accessible to other users either.

{{% notice note %}}
Deleting users does not delete their experiment databases on the file system.
{{% /notice %}}

To delete a user:

* Right-click on the name of the user to be deleted.
* Select {{< icon name="fa-trash">}} **Delete**.

The Delete User dialog will appear:

* Click **Yes**.

---

## Reset all user accounts

A complete reset of all user accounts can be performed by deleting the `admindb` folder in the root folder of the PickCells installation (e.g. `C:\Users\user\PickCells-0.8.0-SNAPSHOT\admindb\`).

{{% notice note %}}
**Reseting all user accounts also removes all the references to all the existing databases**. However, it does not delete the experiment databases on the file system.
{{% /notice %}}

---

## Log off

Click the Log off {{<icon name="new-window">}} icon. This will automatically close all opened experiments and return you to the welcome screen.

{{% notice note %}}
You will not be asked to confirm this action.
{{% /notice %}}
