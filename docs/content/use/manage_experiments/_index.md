+++

title = "Manage experiments"
weight = 2

lastmod = "2018-12-03"

repo = "pickcells-assembly"
fileUrl = "https://framagit.org/pickcellslab/pickcells-assembly/blob/docs/docs/content/use/manage_experiments.md"

+++

## Contents

* [Access the user screen](#access-the-user-screen)
* [Display general information about an experiment](#display-general-information-about-an-experiment)
* [Create a new experiment](#create-a-new-experiment)
* [Edit the description of an experiment](#edit-the-description-of-an-experiment)
* [Load an experiment](#load-an-experiment)
* [Import an existing experiment](#import-an-existing-experiment)
* [Delete an experiment](#delete-an-experiment)
* [Share an experiment](#share-an-experiment)
* [Manually back up an experiment](#manually-back-up-an-experiment)
* [Manually move an experiment](#manually-move-an-experiment)

---

## Access the user screen

To access the user screen at any time, click the Go to the User Overview {{< icon name="fa-user-o" size="large" >}} icon, located on the bottom-left of the PickCells window.

---

## Display general information about an experiment

To display general information about an experiment, hover the mouse over the experiment name. The following information is shown:

* Created by: PickCells user who created the experiment.
* Location: Location of the experiment's database on your file system.
* Description: Short description about the experiment, entered by the user when they created the experiment.

---

## Create a new experiment

To create a new experiment, click the Create a new experiment {{< icon name="fa-lightbulb-o" size="large" >}} icon, located on the right-hand side in the My Experiments panel.

The Experiment Wizard will appear:

* Enter a Name for the experiment (mandatory).
* Click **Browse...** to open up a file browser.
* Browse to a location where you want to hold your experiment database folders.
* Either:
  - Click the Create new folder icon: ![Create new folder icon](/shared/create_new_folder_icon.png?classes=inline&width=30px)
  - Enter a name for the new folder: PickCellsExperiments
  - Click the new folder.
  - Click **Open**
* Or:
  - Click an existing folder.
  - Click **Open**
* The path to your new folder should now be in the Location field.
* Enter a short Description about the experiment (mandatory).
* Click **OK**.

{{% notice note %}} 
Do not put spaces in your experiment name. 
{{% /notice %}} 

A new database will be created on your file system which will be reopened every time you load this experiment.

This folder will have the same name as the experiment and will contain another folder with a name that finishes with `_db`. For example, an experiment called `Sample` will cause the creation of a database folder called `Sample`, with a sub-folder `Sample_db`.

{{% notice note %}}
Do **not** modify the path that leads to the newly created folder.
{{% /notice %}}

---

## Edit the description of an experiment

To edit the description of an experiment:

* Right-click on the experiment name.
* Select {{< icon name="fa-edit" size="large" >}} Edit Description.

The Description Editing... dialog will appear:

* Enter a new description.
* Click **OK**.

---

## Load an experiment

To load an experiment:

* Right-click on the experiment name.
* Select {{< icon name="fa-flask" size="large" >}} Load.

A new workbench, with the experiment's MetaModel View, will appear.

{{% notice note %}}
Double-clicking to load an experiment is **not** supported.
{{% /notice %}}

{{% notice note %}}
It is possible to load multiple experiments. Each experiment is accessible by selecting its tab on the right-hand side of the workbench.
{{% /notice %}}

---

## Import an existing experiment

To import an existing experiment, click the Import an existing experiment {{< icon name="fa-download" size="large" >}} icon, located on the right-hand side in the My Experiments panel.

A file chooser will appear:

* On the left-hand side, navigate to the location of the experiment to be imported and select it.
* The database folder will appear on the right-hand side. It will be a folder name ending in `db`.
* Select the database folder.
* Click **Ok**.

The experiment to be imported will appear in the Managed Users panel under the user name of the user who created the experiment

{{% notice note %}}
The database to import should be located on your local or networked file system and not on an external drive that can be disconnected. PickCells only creates a reference to this database, it does not copy any files.
{{% /notice %}}

---

## Delete an experiment

To delete an experiment:

* Right-click on the experiment name.
* Select {{< icon name="fa-trash" size="large" >}} Delete.

The Select an Option dialog will appear:

* Click **Yes**.

{{% notice note %}}
Deleting experiments does not delete the experiment databases on the file system. It only removes references to the database from within PickCells.

If you are sure that you won't need your database, and other files created within the experiment folder anymore, you can delete the folder manually.
{{% /notice %}}

---

## Share an experiment

It may be convenient to share experiments with other users so that the experiments you create are accessible and editable (if you give permission) by your colleagues from within their own PickCells account.

To share an experiment:

* Right-click on the experiment name.
* Select {{< icon name="fa-share-alt" size="large" >}} Share.

The sharing properties dialog will appear:

* Check Share to share an experiment with a user.
* Uncheck Share to stop sharing an experiment with a user.
* Double-click on the permission field to select the type of sharing:
  - Select READ_ONLY, to allow the user to view the experiment.
  - Select READ_WRITE, to allow the user to view and modify the experiment.
* Click **OK** when done.

Experiments you are sharing will be indicated with a {{< icon name="fa-share-alt" size="large" >}} icon.

Experiments that are shared with you by other users appear in the {{< icon name="fa-users" size="large" >}} Shared With Me panel of the user screen.

---

## Manually back up an experiment

To manually backup an experiment, copy the experiment folder somewhere safe (e.g. to the location where you usually backup files and folders).

---

## Manually move an experiment

To manually move an experiment:

* Manually copy the experiment folder to its new location.
* Within PickCells:
  - Delete the experiment from PickCells.
  - Import the experiment from its new location into PickCells.
  - Check that it has imported correctly.
* Delete the original experiment folder.
