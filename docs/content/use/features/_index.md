+++

title ="Modules"

lastmod = "2018-12-03"

weight = 4

repo = "pickcells-assembly"
fileUrl = "https://framagit.org/pickcellslab/pickcells-assembly/blob/docs/docs/content/use/features/_index.md"

+++

## Analysis modules

PickCells supports the following analysis modules:

| Name | Icon | Description | Tags |
| ---- | :--: | ----------- | ---- |
| Basic Segmentation | ![Basic Segmentation icon](/shared/modules_icons/basic_seg_icon.png?classes=inline&width=40px) | Identify individual objects in images using basic segmentation operations such as thresholding and morphological filtering. | |
| Boundary Extractor | ![Boundary Extractor icon](/shared/modules_icons/boundary_icon.png?classes=inline&width=40px) | Extract distinct boundaries in a segmented image. Boundaries are sections of the contour of objects where the object is touching either the background of the image, the border of the image, or other objects e.g. between cavities and surface. Extracting these bounaries allows them to be used as landmarks for further analysis of the topology of a sample. | |
| Clustering | ![Clustering icon](/shared/modules_icons/clustering_icon.png?classes=inline&width=40px) | Use the DBScan algorithm to cluster objects in space. | |
| Intrinsic Features | ![Intrinsic Features icon](/shared/modules_icons/features_icon.png?classes=inline&width=40px) | Compute intrinsic features of images, particularly segmented shapes, for example intensities or volumes. | |
| Mitosis Detector| ![Mitosis Detector icon](/shared/modules_icons/Mitoses_24.png?classes=inline&width=40px) | Identify Mitosis events in images. | |
| Neighbours Identifier | ![Neighbours Identifier icon](/shared/modules_icons/neighbours_icon.png?classes=inline&width=40px) | Create links between adjacent objects. The maximum distance between adjacent objects and the maximum variation allowed in the distance between neighbouring objects can be selected. The distance and contact area between objects will be stored in the created links. A vector identifying the center of mass of the neighbourhood will also be computed. | |
| NESSYs: Nuclear Envelope Segmentation SYstem  | ![Nessys icon](/shared/modules_icons/RBS_Icon_32.png?classes=inline&width=40px) | Automatically identify cell nuclei in biological images (2D - 3D - 3D + time). Designed to perform well in complex samples i.e when cells are particularly crowded and heterogeneous such as in embryos or in 3D cell cultures. NESSYs is also fast and will work on large images which do not fit in memory. | |
| Network Analysis | ![Network Analysis icon](/shared/modules_icons/network_icon.png?classes=inline&width=40px) | Run a basic network analysis. Given a type of link accessible from Images which connects objects of the same type, compute the degree of each node, the average degree of neighbouring nodes at depth 1 and 2, as well as the local clustering coefficient for each node. | |
| Nuclei Centrosomes Assignment | ![Nuclei Centrosomes Assignment icon](/shared/modules_icons/nc_assign_icon.png?classes=inline&width=40px) | Automatically associate nuclei with centrosomes. This module tries to maximize the number of cells with 1 or 2 centrosomes, but will allow nuclei to have more than 2 centrosomes. A basic classification of the cells is then performed to identify mitotic versus interphase cells versus cells with centrosome amplification. | |
| Object Tracker | ![Object Tracker icon](/shared/modules_icons/tracking_icon.png?classes=inline&width=40px) | Track objects, allowing for divisions, missing objects over several frames and object disappearance. | |
| Overlap (Membership) Finder | ![Overlap (Membership) Finder icon](/shared/modules_icons/overlap_icon.png?classes=inline&width=40px) | Create relationships between segmented objects if their labels overlap. The vector indicating the orientation towards the barycenter of the target is also computed. This can be used, for example, to determine if a specific nucleus belongs to a specific structure. | |
| Relative Connector | ![Relative Connector icon](/shared/modules_icons/relative_icon.png?classes=inline&width=40px) | Calculate relative positions between source objects and a closest target object. These are recorded as connections of type "RELATIVE_TO". A vector for the direction and an anchor point in the mesh of the source and target object is also computed. | |
| [Segmentation Editor](segmentation/seg_editor/) | ![Segmentation Editor icon](/shared/modules_icons/SegEditor_32.png?classes=inline&width=40px) | View and edit segmentation results. View an image with an overlap containing the boundaries of segmented shapes within a segmentation result, correct the segmentation and quantify its accuracy. The number of edits is recorded in a SegmentationResult object. | [segmentation](/tags/segmentation/), [editor](/tags/editor/) |
| Segmented Object Comparator | ![Segmented Object Comparator icon](/shared/modules_icons/compare_icon.png?classes=inline&width=40px) | Creates "BEST MATCH" between segmented objects if their labels overlap by more than 50%. Key differences between the shapes are also computed. | |
| Spots Detector | ![Splots Detector icon](/shared/modules_icons/DotEditor_32.png?classes=inline&width=40px) | Identify bright spots in images, including noisy images, based on the difference of a Gaussian filter. | |
| Spots Editor | ![Spots Editor icon](/shared/modules_icons/DotEditor_32.png?classes=inline&width=40px) | View an image with an overlay containing the location of detected spots and correct and quantify the accuracy of the original detection. The number of edits is stored in the corresponding Image node in the database. | |

## Visualisation modules

PickCells supports the following visualisation modules:

| Name | Icon | Description | Tags |
| ---- | :--: | ----------- | ---- |
| 2D Scatter (Bubble) Plot | ![2D Scatter (Bubble) Plot icon](/shared/modules_icons/bubble_plot_icon.png?classes=inline&width=40px) | Create a 2D scatter (bubble) plot. | |
| 3D Directional Plot | ![3D Directional Plot icon](/shared/modules_icons/Von_mises_fisher.png?classes=inline&width=40px) | Create a 3D directional plot. | |
| 3D Scatter Plot | ![3D Scatter Plot icon](/shared/modules_icons/scatter3d.png?classes=inline&width=40px) | Create a 3D scatter plot. | |
| 3D Scene | ![3D Scene icon](/shared/modules_icons/mesh.png?classes=inline&width=40px) | Display a 3D scene data, if the data can be rendered in 3D. | |
| Histogram | ![Histogram icon](/shared/modules_icons/histo_icon.png?classes=inline&width=40px) | Create a histogram. | |
| Image | ![Image icon](/shared/modules_icons/image_icon.png?classes=inline&width=40px) | Load and view a specific image and select objects in the image. | |
| Pie Chart | {{< icon name="fa-pie-chart" size="40px">}} | Create a pie chart. | |
| Polar Plot | ![Polar icon](/shared/modules_icons/rose_icon.png?classes=inline&width=40px) | Create a polar plot (rose diagram) to display the distribution of angular data. | |
| Time Series Plot | ![Time Series Plot icon](/shared/modules_icons/time_series_icon.png?classes=inline&width=40px) | Create a time series plot. | |
| Track Editor | ![Track Editor icon](/shared/modules_icons/track_editor_icon.png?classes=inline&width=40px) | View and edit tracking outputs. | |
| Tree View | ![Tree View icon](/shared/modules_icons/lineages_icon.png?classes=inline&width=40px) | Display trees for Series Generators. | |
