+++
# This is the Home Page

date = "2018-03-14"
creatorDisplayName = "Arno Proeme"

lastmodifierdisplayname = "Guillaume Blin"
lastmod = "2018-04-20"

repo = "pickcells-assembly"
fileUrl = "https://framagit.org/pickcellslab/pickcells-assembly/blob/docs/docs/content/_index.md"


+++

![PickCells](/pickcells_logo.png)

<div id="discourse-comments" style="float: right; margin-left: 50px; width: 30%;">
<h5 style="color: #B6B6B4;">Latest News</h5>
<script type="text/javascript">
	DiscourseEmbed = {
		discourseUrl : 'https://colony.pickcellslab.org/',
		discourseEmbedUrl : 'http://pickcellslab.frama.io/docs/index.html'
	};

	(function() {
		var d = document.createElement('script');
		d.type = 'text/javascript';
		d.async = true;
		d.src = DiscourseEmbed.discourseUrl + 'javascripts/embed.js';
					(document.getElementsByTagName('head')[0] || document
							.getElementsByTagName('body')[0]).appendChild(d);
				})();
</script>

</div>

### Exploratory Image Analysis for Biology

* Designed for the Quantification of patterning and cell-cell interactions in complex tissues
* Analyse, Visualise and Interrogate from within a unified Graphic User Interface
* Open source and modular framework for Collaborative Development

{{< button href="getstarted/overview/" >}} Overview {{< /button >}}
{{< button href="getstarted/install/" theme="success" >}} Install {{< /button >}}
{{< button href="use/tutorials/" theme="info" >}} User Tutorials {{< /button >}}
{{< button href="developers/plugins/" theme="warning" >}} Developer Tutorials {{< /button >}}
{{< button href="community/contribpolicy/" theme="default" >}} Contribute {{< /button >}}
{{< button href="help/" theme="danger" >}} Get Help {{< /button >}}

<div class= "disclaimer-box">

<p class="disclaimer-title"> Disclaimer </p>
<p class="disclaimer-text">
This website is not yet ready for public use. <br>
The views expressed in this website represent those of the authors, they do not necessarily reflect those of organisations or individuals that support the development of the PickCells software or this website.
</p>

</div>
