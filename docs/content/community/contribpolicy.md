+++

title ="How to Contribute"
weight = 1

date = "2018-04-15"
creatorDisplayName = "Michael Jackson"

lastmodifierdisplayname = "Mike Jackson"
lastmod = "2018-08-02"

repo = "pickcells-assembly"
fileUrl = "https://framagit.org/pickcellslab/pickcells-assembly/tree/docs/docs/content/community/contribpolicy.md"

tags = ["community", "develop"]

+++

Anyone can contribute to, regardless of their skills, as there are many ways to contribute. This page summarises what contributions can be made, how they are made, any conditions the contributions must conform to, and the process followed by the project in accepting the contribution.

All contributions are under the terms of the Contributor Licence Agreement and Certificate of Origin detailed below.

---

## Contents

* [Information on how PickCells has helped you](#information-on-how-pickcells-has-helped-you)
* [Tutorials on using or developing PickCells for research](#tutorials-on-using-or-developing-pickcells-for-research)
* [Requests for changes to the web site or documentation](#requests-for-changes-to-the-web-site-or-documentation)
* [Changes to the web site or documentation](#changes-to-the-web-site-or-documentation)
* [Blog posts](#blog-posts)
* [Feature requests](#feature-requests)
* [New features](#new-features)
* [Bug reports](#bug-reports)
* [Bug fixes](#bug-fixes)
* [Plugins](#plugins)
* [Contributors](#contributors)
* [Contributor Licence-Agreement-and-Certificate-of-Origin](#contributor-licence-agreement-and-certificate-of-origin)


---

## Information on how PickCells has helped you

Share your experiences on the [PickCells Colony forum](https://colony.pickcellslab.org/).

If you are praising any aspect of PickCells we will thank you and ask if we can quote you on our web site.

If you have published a paper, based on research to which PickCells contributed, we will add details about your paper to our [Research](/research/) page.

We may ask if you'd like to write a case study for our web site.

---

## Tutorials on using or developing PickCells for research

Share your experiences on the [PickCells Colony forum](https://colony.pickcellslab.org/).

Provide any source code or data files that are needed by your tutorial.

We will work through your tutorial and check it for readability and correctness. If there are issues, we'll work with you to resolve these.

We will add your tutorial to our [tutorials](/use/tutorials/) page. Your name will be kept on its pages. We will also add you to our [Contributors](#contributors).

---

## Requests for changes to the web site or documentation

If you want to request changes to the web site or documentation, you can do one of:

* Share on the [PickCells Colony forum](https://colony.pickcellslab.org/).
* Raise an issue in the [issue tracker](https://framagit.org/groups/pickcellslab/-/issues).

We will make a decision as to whether to make the changes requested.

---

## Changes to the web site or documentation

If you want to submit changes to the web site or documentation, you can:

* Submit a merge request with the changes to be made. See [Using Git and Framagit](/developers/dev_core/day_to_day/#using-git-and-framagit) and [Use merge requests to request PickCells maintainers review and merge your branch](/developers/dev_core/day_to_day/#use-merge-requests-to-request-pickcells-maintainers-review-and-merge-your-branch).

We will review submitted changes. If there are issues, we'll work with you to resolve these. Once complete, we will merge your changes into PickCells and add you to our [Contributors](#contributors).

---

## Blog posts

When you publish anything related to PickCells in your own blog: Let us know about it so we can refer to it.

Share your blog post link on the [PickCells Colony forum](https://colony.pickcellslab.org/).

If you are praising any aspect of PickCells we will thank you and ask if we can quote you on our web site.

We will add a link to your blog post on our [Research](/research/) page.

---

## Feature requests

If you want to request a new feature, first check that the feature does not already exist and is not currently planned: 

* Check the [feature request](https://colony.pickcellslab.org/c/feature-request)s in the [PickCells Colony forum](https://colony.pickcellslab.org/).
* Check the [issue tracker](https://framagit.org/groups/pickcellslab/-/issues).
* Check the PickCells [milestones](https://framagit.org/dashboard/milestones) in Framagit. 

If you want to request a feature, you can do one of:
 
* Submit a [feature request](https://colony.pickcellslab.org/c/feature-request) to the [PickCells Colony forum](https://colony.pickcellslab.org/).
* Raise an issue in the [issue tracker](https://framagit.org/groups/pickcellslab/-/issues).

We aim to acknowledge feature requests, encourage participation in implementing features, and to explain cases where a decision is made not to implement a feature at a specific point in time. 

For advice on how to write a good feature request, see [How do I write a good feature request](https://meta.stackexchange.com/questions/258136/how-do-i-write-a-good-feature-request). 

---

## New features

For plugins see [plugins](#plugins) below.

If you want to submit a feature you have developed, make sure that:

* Your code confirms, as far as possible, to the [coding standards](/developers/collab_dev/#coding-standards).
* You have written automated tests (either new tests, or updates to existing tests) to test your feature.
* You have a list of steps required to demonstrate your feature.
* You have made any changes required to the web site documentation that are incurred by your feature.

You can then:

* Submit a merge request with the changes to be made. See [Using Git and Framagit](/developers/dev_core/day_to_day/#using-git-and-framagit) and [Use merge requests to request PickCells maintainers review and merge your branch](/developers/dev_core/day_to_day/#use-merge-requests-to-request-pickcells-maintainers-review-and-merge-your-branch).

We will thank you for your effort, will check that your code compiles, that your code conforms, as far as possible, to the [coding standards](/developers/collab_dev/#coding-standards), and that your tests pass. We will also review your code. We will run though the list of steps to test your feature and validate any documentation you provide. If there are issues, we'll work with you to resolve these.

Once complete, we will merge your code into PickCells and add you to our [Contributors](#contributors).

---

## Bug reports

If you want to report a bug, first check that the bug has not already been reported: 

* Search the documentation on this web site.
* See the [frequently asked questions](/help/#frequently-asked-questions-faq).
* Search the [PickCells Colony forum](https://colony.pickcellslab.org/), in particular the [bugs](https://colony.pickcellslab.org/c/bugs) and [how-to](https://colony.pickcellslab.org/c/how-to) categories.
* Search the [issue tracker](https://framagit.org/groups/pickcellslab/-/issues) for known problems and bugs (both fixed and unfixed). Be sure to search "All" issues, both "Open" and "Closed".

If you want to report bug, you can do one of:

* Either, ask a question on the [PickCells Colony forum](https://colony.pickcellslab.org/) in the [bugs](https://colony.pickcellslab.org/c/bugs) or [how-to](https://colony.pickcellslab.org/c/how-to) categories.  
* Or, raise an issue in the [issue tracker](https://framagit.org/groups/pickcellslab/-/issues).
 
We aim to acknowledge bug reports, encourage participation in resolving bugs, and to explain cases where a decision is made not to fix a bug at a specific point in time. 
 
---

## Bug fixes

If you want to submit a bug fix you have implemented make sure that:

* Your code confirms, as far as possible, to the [coding standards](/developers/collab_dev/#coding-standards).
* You have written a regression test beforehand, making sure that it fails in the presence of the bug, and, after the bug has been fixed, that it passes. 
* You have made any changes required to the web site documentation that are incurred by your bug fix.

You can then:

* Submit a merge request with the changes to be made. See [Using Git and Framagit](/developers/dev_core/day_to_day/#using-git-and-framagit) and [Use merge requests to request PickCells maintainers review and merge your branch](/developers/dev_core/day_to_day/#use-merge-requests-to-request-pickcells-maintainers-review-and-merge-your-branch).

We will thank you for your effort, will check that your code compiles, that your code conforms, as far as possible, to the [coding standards](/developers/collab_dev/#coding-standards), and that your tests pass. We will also review your code. We will run though the list of steps to test your feature and validate any documentation you provide. If there are issues, we'll work with you to resolve these.

Once complete, we will merge your code into PickCells and add you to our [Contributors](#contributors).

---

## Plugins

If you have [created your own plugin](/developers/plugins/) share a link to it on the [PickCells Colony forum](https://colony.pickcellslab.org/).

We will thank you for your effort. 

You will remin responsible for its maintenance, testing and licensing.

---

## Contributors

The following people have contributed to this code under the terms of the Contributor Licence Agreement and Certificate of Origin detailed below:

* None

---

## Contributor Licence Agreement and Certificate of Origin

```
By making a contribution to this project, I certify that:

(a) The contribution was created in whole or in part by me and I have
    the right to submit it, either on my behalf or on behalf of my
    employer, under the terms and conditions as described by this file;
    or

(b) The contribution is based upon previous work that, to the best of
    my knowledge, is covered under an appropriate licence and I have
    the right or permission from the copyright owner under that licence
    to submit that work with modifications, whether created in whole or
    in part by me, under the terms and conditions as described by
    this file; or

\(c\) The contribution was provided directly to me by some other person
    who certified (a) or (b) and I have not modified it.

(d) I understand and agree that this project and the contribution
    are public and that a record of the contribution (including my
    name and email address) is maintained indefinitely and may be
    redistributed consistent with this project or the licence(s)
    involved.

(e) I, or my employer, grant all recipients of this software a
    perpetual, worldwide, non-exclusive, no-charge, royalty-free,
    irrevocable copyright licence to reproduce, modify, prepare
    derivative works of, publicly display, publicly perform,
    sub-licence, and distribute this contribution and such
    modifications and derivative works consistent with this project or 
    the licence(s) involved or other appropriate open source
    licence(s) specified by the project and approved by the 
    [Open Source Initiative (OSI)](http://www.opensource.org/), for
    code, or other appropriate [Creative
    Commons](http://creativecommons.org/) licence(s) specified by the
    project, for documentation.

(f) If I become aware of anything that would make any of the above
    inaccurate, in any way, I will let you (via the PickCells forum,
    https://colony.pickcellslab.org/ or by emailing
    TODO as soon as I become aware.

(The PickCells Contributor Licence Agreement and
Certificate of Origin is inspired by the UK Met Office FCM Contributor
Licence Agreement and Certificate of Origin which was, in turn,
inspired by the Certificate of Origin used by Enyo and the Linux
Kernel.)
```
