+++

title ="Research"
weight = 5

date = "2018-07-23"
creatorDisplayName = "Guillaume Blin"

lastmodifierdisplayname = "Mike Jackson"
lastmod = "2018-08-02"

repo = "pickcells-assembly"
fileUrl = "https://framagit.org/pickcellslab/pickcells-assembly/blob/docs/docs/content/research/_index.md"  

tags = ["impact"]

+++

Publications where PickCells has been used:

  * __2018__  
    * [Geometrical confinement controls the asymmetric patterning of Brachyury in cultures of pluripotent cells](http://dev.biologists.org/content/145/18/e1801). Blin G., Wisniewski D, Picart C, Thery M, Puceat M, Lowell S. Development. 2018 Aug 16. pii: dev.166025. doi: 10.1242/dev.166025. [Epub ahead of print] PMID: 30115626 

  * __2017__  
    * [Polarity Reversal by Centrosome Repositioning Primes Cell Scattering during Epithelial-to-Mesenchymal Transition](http://dev.biologists.org/content/145/18/dev166025.long). Burute M, Prioux M, Blin G, Truchet S, Letort G, Tseng Q, Bessy T, Lowell S, Young J, Filhol O, Théry M.Dev Cell. 2017 Jan 23;40(2):168-184. doi: 10.1016/j.devcel.2016.12.004. Epub 2016 Dec 29.

  * __2016__
    * [Position-dependent plasticity of distinct progenitor types in the primitive streak](https://www.ncbi.nlm.nih.gov/pubmed/26780186) Wymeersch FJ, Huang Y, Blin G, Cambray N, Wilkie R, Wong FC, Wilson V. Elife. 2016 Jan 18;5:e10042. doi: 10.7554/eLife.10042.
