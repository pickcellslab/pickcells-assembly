+++

title ="Overview"
weight = 2

date = "2018-04-16"
creatorDisplayName = "Guillaume Blin"

lastmodifierdisplayname = "Guillaume Blin"
lastmod = "2018-09-21"

repo = "pickcells-assembly"
fileUrl = "https://framagit.org/pickcellslab/pickcells-assembly/blob/docs/docs/content/introduction/overview.md"

+++


## PickCells in a few words

PickCells is dedicated to the analysis of multidimensional biological images. It is a stand-alone desktop application with a graphical user interface (GUI) written in Java. 

It is particularly suited for the quantification and exploration of 3D spatial relationships between objects of complex biological systems such as stem cell niches, organoids, embryos, etc...

[//]: # (TODO Add image carousel here)

---

## Main Goals

Our main goal with PickCells is to create a tool which can help both the biologist and developer-analyst sides of the bio-image analysis community. To this end we are exploring three inter-related axes:

---

### 1. A powerful graphical user interface

We want to provide a tool enabling biologists to conduct sophisticated image analysis within a unified graphical user interface (GUI).

To this end, **the GUI gives access to libraries** of analysis methods (object detection, spatial analyses, etc...) as well as interactive visualisations (Images with overlays, 3D scenes, charts, etc...) which can also be used to validate and curate the data. We call elements of these libraries ['Modules'](https://en.wiktionary.org/wiki/module).

Most importantly, **PickCells connects modules together**. When a task is performed within PickCells, the data that are generated are stored in [a database](https://en.wikipedia.org/wiki/Database) which can be read and understood by all the other modules even when modules have been created by developers who know only very little about each other. For example when we detect nuclei in images, the nuclei can be readily loaded into a 3D scene or chart or used by another module which can measure how the cells are spatially organised.

This is important because this means that there is **no more need to deal with heterogeneous data formats** when building a workflow. This makes the task less tedious and decreases the risk of errors. 

Workflows are generally built around specific biological questions and what matters at the end of the day is the possibility to **interrogate the data in a meaningful and flexible manner**.  

For this purpose, we have included intelligent graphical query builders. These allow the users to create subsets, categories or new properties for any type of data that are generated.  

[//]: # (TODO link to query builder post)

Again, the definition of these **annotations are standard** across the application such that other modules can leverage them. For example anatomical regions of an embryo may have been grouped into categories. Once these categories are created, an analysis module can compute the relative distances of nuclei with respect to each category of anatomical landmarks. Possibilities are broad and customisable according to the biological question.

[//]: # (TODO - > future directions (reproducible and sharable workflows))

---

### 2. A modular and extensible framework

PickCells is a **modular and extensible framework** with a **rich API for developers**. The programming model ensures that modules remain as generic as possible and have access to generic features of the application without direct dependencies on the specific implementation (Please see our [post on dependency injection](/developers/architecture/foundationj/dependency_injection/) for a discussion on modular design - mainly aimed at developers).

PickCells allows developer-analysts to:

* Develop plugins that can be directly integrated into the platform and readily usable within a workflow.
* Speed-up the development cycle of a new method by taking advantage of all the available viualisations and libraries
* Disseminate their methods within the PickCells software and websites (see also [our forum](https://colony.pickcellslab.org/)).

---

### 3. An online infrastructure for collaborative evolution

We have adopted a [FLOSS](https://fsfe.org/about/basics/freesoftware.en.html) (Free/Libre Open Source Software) model for PickCells. 

We have established the foundations of a collaborative framework which we now hope is solid and helpful enough to be adopted by the community. Now that these foundations are laid, we think that the most effective manner to let this initiative flourish is to create an ecosystem which enables collaborative planning and development, so that the future of this project belongs to and benefits the community.

For this purpose, we have established online tools surrounding PickCells to ease migration towards the self-organisation of the PickCells user and developer community:

* Public website: Documentation is essential to the sustainability and adoptability of any free software (see GNU's [Why Free Software needs Free Documentation](https://www.gnu.org/philosophy/free-doc.html)). The documentation on this website is free and anyone can contribute extensions or enhancements.
* Open source code repositories: Anyone can read, analyse, understand, fork and contribute to PickCells via our publicly accessible [source code repositories](https://framagit.org/pickcellslab).
* Issue tracker: PickCells repositories are complemented by a publicly accessible [issue tracker](https://framagit.org/groups/pickcellslab/-/boards) which can help developers to organise current and future work.
* Discussion forum: [PickCells Colony forum](https://colony.pickcellslab.org/) allows for communications within the community. It serves as:
  - A collaborative knowledge base.
  - A bridge between the user and developer community for mutual help.
  - A collective decision-making platform.

For information on how to get involved see [Collaborative development](/developers/collabdev/) and [How to contribute](/community/contribpolicy/).

Some of this collaborative infrastructure resembles what is being done around the [Fiji](http://fiji.sc/) and, more broadly, the [Scijava](http://scijava.org/) projects. We are indeed inspired by this work and hope that PickCells will grow alongside such well established and successful projects.

---

## Publications, presentations and posters

For information on publications presentations and posters about research done using PickCells, see the [Research](/research/) page.
