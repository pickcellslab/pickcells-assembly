+++

title = "Install PickCells"
weight = 4

lastmod = "2018-10-10"

repo = "pickcells-assembly"
fileUrl = "https://framagit.org/pickcellslab/pickcells-assembly/blob/docs/docs/content/getstarted/install.md"

+++

## Supported operating systems

Currently PickCells is supported on the following operating systems:

* Ubuntu (tested on Ubuntu 16.04.2 LTS)
* Other flavours of Linux
* Windows (tested on Windows 7 service pack 1 and Windows 10)

---

## Install Java 1.8 JRE

To install Java may require that you have administrator access to install and configure software. If you do not have administrator access, a local system administrator may be able to give you this, or install Java for you.

You can use either [OpenJDK](http://openjdk.java.net) or [Oracle Java](https://www.oracle.com/uk/java/index.html). 

64-bit Java is highly recommended for 64-bit operating systems.

PickCells has not been tested on Java 1.9.

For full installation instructions for Java, consult the relevant third-party documentation.

### Install OpenJDK

**Ubuntu users**

Run:

```
sudo apt-get install openjdk-8-jre
java -version
```

You should see something like:

```
openjdk version "1.8.0_131"
OpenJDK Runtime Environment (build 1.8.0_131-8u131-b11-2ubuntu1.16.04.3-b11)
OpenJDK 64-Bit Server VM (build 25.131-b11, mixed mode)
```

### Install Oracle Java

**Windows users**

* Visit Oracle [Java SE Runtime Environment 8 Downloads](http://www.oracle.com/technetwork/java/javase/downloads/jre8-downloads-2133155.html).
* Click Accept License Agreement.
* Download Windows x64 version e.g. `jre-8u144-windows-x64.exe`.
* Double-click `exe` e.g. `jre-8u144-windows-x64.exe`.
* Follow installer instructions.
* Open a command prompt.
* Run:

```
java -version
```

* You should see something like:

```
java version "1.8.0_144"
Java(TM) SE Runtime Environment (build 1.8.0_144-b01)
Java HotSpot(TM) 64-Bit Server VM (build 25.144-b01, mixed mode)
```

---

## Install PickCells

Download the latest distribution of PickCells from the University of Edinburgh [ownCloud](https://datasync.ed.ac.uk/index.php/s/VdTV5V5PmEWDc07) server (password is 'pickcells').

The current Version is 'PickCells-0.8.0-SNAPSHOT.zip'.

**Ubuntu users**

Run:

```
mkdir PickCells-0.8.0-SNAPSHOT
mv PickCells-0.8.0-SNAPSHOT.zip PickCells-0.8.0-SNAPSHOT
cd PickCells-0.8.0-SNAPSHOT
unzip PickCells-0.8.0-SNAPSHOT.zip
```

Move `PickCells-0.8.0-SNAPSHOT` to the folder of your choice **before you first run it**.

**Windows users**

1. Right-click `PickCells-0.8.0-SNAPSHOT.zip`.
2. Select Extract All...
3. Move `PickCells-0.8.0-SNAPSHOT` to the folder of your choice **before you first run it**.


## Run PickCells

You are now ready to [Run PickCells](/getstarted/run).
