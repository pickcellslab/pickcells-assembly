+++

title = "Map of the PickCells user interface"

weight = 6

lastmod = "2018-12-04"

repo = "pickcells-assembly" 
fileUrl = "https://framagit.org/pickcellslab/pickcells-assembly/blob/docs/docs/content/getstarted/gui.md" 
+++

## Contents

* [PickCells screens](#pickcells-screens)
* [Welcome screen](#welcome-screen)
* [Sandbox](#sandbox)
* [User screen](#user-screen)
* [Workbench](#workbench)
* [Status bar](#status-bar)

---

## PickCells screens

The PickCells window displays four main screens, which are depicted in the diagram below:

{{<mermaid align="left">}}

graph TD
A(fa:fa-home Welcome Screen) --> B(fa:fa-cubes Sandbox)
A -->|Login| C(fa:fa-user-circle User Screen)
C -->|Load experiment| D(fa:fa-flask Workbench)

C -->|Log off| A
D -->|Log off| A

style A fill:#fbfad0,stroke:black,stroke-width:1px
style B fill:#f6f289,stroke:black,stroke-width:1px
style C fill:#f6f289,stroke:black,stroke-width:1px
style D fill:#f2ed5a,stroke:black,stroke-width:1px

{{</mermaid>}}

Navigation between each screen is via the icons which are located at the bottom-left of the PickCells window, which match those shown in the diagram above:

![Navigation icons](/getstarted/screens/screen_icons.png?classes=left,border,shadow)

The Log off {{<icon name="new-window">}} icon closes the user screen or workbench and returns you to the welcome screen.

---

## Welcome screen

The **welcome screen** provides general information about PickCells.

![Welcome screen](/getstarted/screens/welcome_screen.png?classes=left,border,shadow)

From the welcome screen, you can:

* Click **Sandbox** to go to the [sandbox](#sandbox).
* Click **Login** to login. Once logged in, you will be taken to the [user screen](#user-screen).

---

## Sandbox


The **sandbox** provides you with access to a collection of tools for image processing and visualisation. These are generic tools and don't extract any specific data or update your experiment database.

![Sandbox](/getstarted/screens/sandbox.png?classes=left,border,shadow)

---

## User screen

The **user Screen** is where you manage experiments (create, delete, share, ...). If you are an administrator, this is where you can also manage other users (add, remove, ...).

![User screen](/getstarted/screens/user_screen.png?classes=left,border,shadow)

For more information, please see:

* [Manage Experiments](/use/manage_experiments)
* [Manage Users](/use/manage_users)

---

## Workbench

The **workbench** is the heart of PickCells. When you load an experiment, a corresponding workbench is opened. Each experiment's workbench provides you with a view of the metadata of that experiment, and allows you to carry out myriad analysis and visualisation tasks. 

![Workbench](/getstarted/screens/workbench.png?classes=left,border,shadow)

The key components of the workbench include:

* **Analysis modules**: The icons on the left-hand side provide access to analysis modules. There is one icon for each module.
  - Click on an icon to open the corresponding analysis module.
  - See [Modules](/use/features) for more information on each module.
* **Visualisation modules**: The icons along the top provide access to visualisation modules. There is one icon for each module.
  - Click on an icon to open the corresponding visualisation module.
  - See [Modules](/use/features) for more information on each module.
* **MetaModel View**: a visualisation of the current state of an experiment, as held within the database. This shows the experiment's objects and the relationships between these.
  - Click on a node to centre the view on that node.
  - Right-click on a node to open a popup menu which allows you to get more information about the node or delete the associated object.
  - Enter text into the **search** box to highlight nodes whose names match that text.
* **Loaded experiments**: the tabs on the right-hand side show the currently-loaded experiments. 
  - Click on a tab to view the workbench for the corresponding experiment.

{{% notice note %}}
Analysis and visualisation modules can only be used if the database holds the information they require to operate. If it does not, then PickCells will display an error dialog.
{{% /notice %}}

---

## Status bar

At the bottom of the PickCells window is the status bar:

![Status bar](/getstarted/screens/status_bar.png?classes=left,border,shadow)

The status bar includes:

* **Heap status**: Indicates how much heap (or memory) PickCells is currently using. In the above example, '124 MB of 356 GB'.
* **Navigation icons**: to navigate between screens, as described above in [PickCells screens](#pickcells-screens).
* **PickCells version number**.
