+++

title = "Run PickCells"
weight = 5

lastmod = "2018-10-11" 
 
repo = "pickcells-assembly" 
fileUrl = "https://framagit.org/pickcellslab/pickcells-assembly/blob/docs/docs/content/getstarted/run.md" 

+++

## Start PickCells

**Ubuntu users**

Open a terminal window.

Run:

```
chmod +x startup.sh
./startup.sh
```

**Windows users**

Double-click `startup.bat`.


---

## Troubleshooting: PickCells does not launch

**Ubuntu users**

Errors are displayed in the terminal window in which you ran `./startup.sh`.

**Windows users**

1. Right click in the folder which contains `startup.bat` and choose Open command window here.
2. If you do not have this option, type `cmd` in the Windows Explorer text field displaying the current path and press enter.
3. In the command prompt, run:

```
startup.bat
```

Errors are displayed in the command prompt window.

---

## Troubleshooting: "The specified size exceeds the maximum representable size"

If, on Windows, you run:

```
startup.bat
```

and get an error like:

```
java -Xms1024m -Xmx4g -cp "bin/*;prov/*;plugin/*;lib/*" -Dlog4j.configuration=log4j.properties org.pickcellslab.foundationj.bootstrap.Bootstrap /resources/Laras.jpg

Invalid maximum heap size: -Xmx4g

The specified size exceeds the maximum representable size.

Error: Could not create the Java Virtual Machine.

Error: A fatal exception has occurred. Program will exit.
```

Then:

* See [Update maximum memory allocated to PickCells](#update-maximum-memory-allocated-to-pickcells) below.
* Or, if using 32-bit Java, upgrade to 64-bit Java.

---

## Update maximum memory allocated to PickCells

By default, the maximum memory allocated to PickCells is 8GB.

If you are running a 32-bit operating system, or have 32-bit Java, the maximum amount of memory you can allocate is 2GB. To check which version of Java you have, see "Check your Java version" as described below.

To update the maximum memory allocated:

1. Open the startup script in a text editor:
   - Ubuntu users: open `startup.sh`.
   - Windows users: open `startup.bat`.
2. Edit the `-Xmx8g` setting to specify a new memory allocation:
   - `8g` means 8GB.
   - To reduce this to 2GB, update the setting to `-Xmx2g`.
   - To reduce this to 4GB, update the setting to `-Xmx4g`.
3. Save your changes.

---

## Check your Java version

You can check your Java version at the command prompt.

Run:

```
java -version
```

Typically, if 64 bit Java is installed, then this will be printed. For example, for OpenJDK:

```
OpenJDK 64-Bit Server VM (build 25.131-b11, mixed mode)
```

For Oracle Java:

```
Java HotSpot(TM) 64-Bit Server VM (build 25.144-b01, mixed mode)
```
