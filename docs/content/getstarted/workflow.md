+++

title ="Image Analysis"
weight = 1

draft=true

date = "2018-04-16"
creatorDisplayName = "Guillaume Blin"

lastmodifierdisplayname = "Guillaume Blin"
lastmod = "2018-09-21"

repo = "pickcells-assembly"
fileUrl = "https://framagit.org/pickcellslab/pickcells-assembly/blob/docs/docs/content/introduction/workflow.md"

+++


# TODO

This page will provide an overview of the requirements of Image Analysis. What it is, why is it needed, what are the requirements, some challenges and how PickCells can help.

# Contents

1. Introduction
2. Major hurdles
3. Other Resources
