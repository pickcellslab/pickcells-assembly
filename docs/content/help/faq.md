+++

title ="Frequently Asked Questions (FAQ)"
weight = 1
 
lastmod = "2018-12-04"
 
repo = "pickcells-assembly" 
fileUrl = "https://framagit.org/pickcellslab/pickcells-assembly/tree/docs/docs/content/help/faq.md" 
 
tags = ["help"] 

+++

## Can I import images to a PickCells experiment which already contains images?

Although there is an opened discussion on whether or not to enable this feature, the current answer to this question is no you can't.  

One of the reasons is that a PickCells experiment corresponds to the analysis of images obtained from a **single** biological experiment where all images are **comparable**, i.e. acquired on the same microscope with the same settings. It is assumed that when you create a PickCells experiment, you already have all your images ready for analysis.

Said differently, one image in a PickCells experiment can be seen as one data point in a biological experiment. Very much like in a biological experiment, data points cannot be added after the experiments has been performed mainly because data points from 2 distinct experiments are unlikely to be directly comparable. If more data points are needed then the way to go instead is to replicate the experiment (a new biological experiment in the lab and a new corresponding experiment in PickCells).  

Please see also [What is an 'experiment' in PickCells?](/getstarted/basic_concepts/#what-is-an-experiment-in-pickcells) and [the pipeline builder idea](https://framagit.org/pickcellslab/pickcells-api/issues/3) which could be used in the context of experimental replicates.

---

## Question: I am getting errors when importing files larger than 4GB into PickCells. How do I solve this issue?

There is currently a problem in the ImageJ I/O library that PickCells is using to import images. See the ImageJ issue [Improve ImageJ's handling of large (>4GB) TIFF files](https://github.com/imagej/imagej/issues/117).

In order to circumvent this problem, you can use [ImageJ](https://imagej.nih.gov/ij/) to open your file and export the image as an ICS file. To do so, use Plugins ->BioFormats-Exporter and export as `ics`. Make sure to add the file extension `.ics` to your file name. .
