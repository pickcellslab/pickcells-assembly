+++

title ="Get Help"
weight = 6
 
lastmod = "2018-12-04"
 
repo = "pickcells-assembly" 
fileUrl = "https://framagit.org/pickcellslab/pickcells-assembly/tree/docs/docs/content/help/_index.md" 
 
tags = ["help"] 

+++

## Get Help

If you experience a problem when using PickCells, and need help, then please check if your issue has already been resolved or your question has already been answered:

* Search the documentation on this web site. 
* See the [frequently asked questions](faq).
* Search the [PickCells Colony forum](https://colony.pickcellslab.org/), in particular the [bugs](https://colony.pickcellslab.org/c/bugs) and [how-to](https://colony.pickcellslab.org/c/how-to) categories. 
* Search the [issue tracker](https://framagit.org/groups/pickcellslab/-/issues) for known problems and bugs (both fixed and unfixed). Be sure to search "All" issues, both "Open" and "Closed". 

If you cannot find a solution, then you can do one of:

* Either, ask a question on the [PickCells Colony forum](https://colony.pickcellslab.org/) in the [bugs](https://colony.pickcellslab.org/c/bugs) or [how-to](https://colony.pickcellslab.org/c/how-to) categories. 
* Or, raise an issue in the [issue tracker](https://framagit.org/groups/pickcellslab/-/issues).

See [How to ask for help](#how-to-ask-for-help), below, for advice on what information you should provide.

If you need to contact the PickCells maintainers directly, then email TODO.

### What is and is not supported

Only binary releases of PickCells with a version number, this web site, the PickCells Colonly forum, and the PickCells project on Framagit are supported.

Modified versions of PickCells released by third-parties are not supported.

### What support we offer

The PickCells team consists of members in a number of organisations who work on a number of projects, and may be very busy. PickCells support relies upon the goodwill of these members and organisations.

### How to ask for help

* Submit only one help request per report.
* State your platform, operating system and version.
* State the version of PickCells you are using.
* Provide any error messages and exceptions that you see. 
  - If providing exceptions then provide these exactly as displayed in a terminal window. 
  - Do **not** provide a simple textual summary like 'It threw an error'. 
  - If using a graphical user interface, then see the next point.
* If using any graphical user interface and, if you think it would help, then send a copy of your display: 
  - Under Ubuntu, use the Print Screen key to save an image of your display into your home directory as a .png file. 
  - Under Windows, click CTRL and Print Screen, select **Start** => **All Programs** => **Accessories** => **Paint**, click CTRL and V to paste the image into Paint and then Save the image as a .png or .jpg file.
* If possible, provide a recipe for repeating a bug.
* Provide any other information you think will help.
* Clearly separate fact from speculation.

Requests of the form "I used PickCells and am stuck" are not helpful to either you or the PickCells maintainers.

No bug is too trivial to report - small bugs may hide big bugs.
