#!/bin/bash
dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
export CLASSPATH=$CLASSPATH:"$dir"/bin/*:"$dir"/lib/*:"$dir"/prov/*:"$dir"/bin/*:"$dir"/plugins/*
echo $CLASSPATH
java -Xms1024m -Xmx4096m org.pickcellslab.foundationj.bootstrap.BootStrapper /resources/Laras.jpg
