#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $DIR
java -Xms1024m -Xmx12g -classpath $CLASSPATH':lib/*:bin/*:prov/*:plugins/*' org.pickcellslab.foundationj.bootstrap.BootStrapper /resources/Laras.jpg
